export class PipelineError extends Error {
  private constructor(message) {
    super(message);
  }

  static throw(message: any, ...params: any[]) {
    throw new PipelineError({ message, params });
  }
}
