import { Controller, Get, Render, Request, UseGuards } from '@nestjs/common';
import { AuthenticatedSessionGuard } from 'src/common/guards/authenticated-session.guard';
@UseGuards(AuthenticatedSessionGuard)
@Controller('schedule')
export class ScheduleController {
  @Get()
  @Render('schedule')
  public root(@Request() req) {
    return {
      message: req.flash('message'),
      token: req.user.token,
      year: new Date().getFullYear(),
    };
  }
}
