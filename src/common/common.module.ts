import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { LocalStrategy } from './local.strategy';
import { SisandService } from './services/sisand.service';
import { SessionSerializer } from './session.serializer';

@Module({
  imports: [PassportModule, HttpModule],
  providers: [LocalStrategy, SessionSerializer, SisandService],
  exports: [SisandService],
})
export class CommonModule {}
