import { Column, Entity, PrimaryColumn } from 'typeorm';

export interface CompanyData {
  code: number;
  name: string;
  alias: string;
}

@Entity({ name: 'ERP_API_EMPRESA' })
export class Company {
  @PrimaryColumn({ name: 'CDEMPRESA' })
  code: number;

  @Column({ name: 'NMFANTASIA' })
  name: string;

  @Column({ name: 'DSAPELIDO' })
  alias: string;

  static factory(data: CompanyData): Company {
    const company = new Company();
    company.update(data);
    company.code = data.code;
    return company;
  }

  update?(data: CompanyData): Company {
    this.name = data.name;
    this.alias = data.alias;
    return this;
  }
}
