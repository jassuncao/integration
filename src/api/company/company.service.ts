import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { extractToken } from 'src/common/authorization.helper';
import { SisandService } from 'src/common/services/sisand.service';
import { Repository } from 'typeorm';
import { Company } from './company.entity';

@Injectable()
export class CompanyService {
  constructor(
    @InjectRepository(Company)
    private readonly companyRepository: Repository<Company>,
    private readonly sisandService: SisandService,
  ) {}

  public async sync(authorization: string): Promise<void> {
    const token = extractToken(authorization);
    const data = await this.sisandService.getCompanies(token);
    await this.syncCompany(data);
  }

  private syncCompany(data: any[]): Promise<Company[]> {
    const companies = data
      .map((data) => ({
        code: data.CDEMPRESA,
        name: data.NMFANTASIA,
        alias: data.DSAPELIDO,
      }))
      .map(Company.factory);
    return this.companyRepository.save(companies);
  }

  public async getAll(): Promise<Company[]> {
    return this.companyRepository.find();
  }
}
