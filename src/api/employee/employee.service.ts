import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { extractToken } from 'src/common/authorization.helper';
import { SisandService } from 'src/common/services/sisand.service';
import { Repository } from 'typeorm';
import { Company } from '../company/company.entity';
import { CompanyService } from '../company/company.service';
import { Employee } from './employee.entity';

type SyncEmpolyee = (company: Company) => Promise<Employee[]>;

@Injectable()
export class EmployeeService {
  constructor(
    @InjectRepository(Employee)
    private readonly employeeRepository: Repository<Employee>,
    private readonly companyService: CompanyService,
    private readonly sisandService: SisandService,
  ) {}

  public async sync(authorization: string): Promise<void> {
    const companies = await this.companyService.getAll();
    const token = extractToken(authorization);
    await Promise.all(companies.map(this.syncEmployee(token)));
  }

  private syncEmployee = (token: string): SyncEmpolyee => {
    return async ({ code }) => {
      const data = await this.sisandService.getEmployees(code, token);
      const employees = data
        .map((data) => ({
          code: data.CDVENDEDOR,
          name: data.NMVENDEDOR,
          alias: data.NMAPELIDOVENDEDOR,
          company: data.CDEMPRESA,
        }))
        .map(Employee.factory);
      return this.employeeRepository.save(employees);
    };
  };
}
