import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CompanyCreate1651801506527 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'ERP_API_EMPRESA',
        columns: [
          {
            name: 'CDEMPRESA',
            type: 'integer',
            isPrimary: true,
          },
          {
            name: 'NMFANTASIA',
            type: 'varchar(200)',
          },
          {
            name: 'DSAPELIDO',
            type: 'varchar(50)',
          },
        ],
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('ERP_API_EMPRESA');
  }
}
