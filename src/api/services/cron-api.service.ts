import { Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { endOfWeek, startOfWeek } from 'date-fns';
import { SisandService } from 'src/common/services/sisand.service';
import { CompanyService } from '../company/company.service';
import { EmployeeService } from '../employee/employee.service';
import { ScheduleService } from '../schedule/schedule.service';

@Injectable()
export class CronApiService {
  private readonly logger = new Logger(CronApiService.name);

  constructor(
    private readonly sisandService: SisandService,
    private readonly companyService: CompanyService,
    private readonly employeeService: EmployeeService,
    private readonly scheduleService: ScheduleService,
  ) {}

  @Cron(CronExpression.EVERY_DAY_AT_6AM)
  async handleCron() {
    this.logger.log('*** Start sync SISAND ***');
    const token = await this.sisandService.getToken('despachante', 'LasTUNcH');
    await this.companyService.sync(token);
    await this.employeeService.sync(token);
    await this.scheduleService.sync(
      { startDate: startOfWeek(new Date()), endDate: endOfWeek(new Date()) },
      token,
    );
    this.logger.log('*** End sync SISAND ***');
  }
}
