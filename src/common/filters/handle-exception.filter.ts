import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
} from '@nestjs/common';
import { HttpArgumentsHost } from '@nestjs/common/interfaces';
import { Request, Response } from 'express';
import { startsWith } from 'lodash';
import {
  EXCEPTION_BUILDERS,
  HandleBuildException,
  INTERNAL_ERROR,
} from './handlers/exception';
import { ExceptionResponse } from './handlers/exception-builder';

export interface IRequestFlash extends Request {
  flash: any;
}

@Catch(HttpException)
export class HandleExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const builder = EXCEPTION_BUILDERS.find((e) => e.accept(exception));
    this.handle(builder || INTERNAL_ERROR, exception, ctx);
  }

  private handle(
    { build }: HandleBuildException,
    exception: HttpException,
    ctx: HttpArgumentsHost,
  ) {
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<IRequestFlash>();
    const resolve = build(exception);

    if (startsWith(request.originalUrl, '/api')) {
      this.httpResolve(response, resolve);
    } else {
      this.sessionResolve(request, response, resolve);
    }
  }

  public sessionResolve(
    request: IRequestFlash,
    response: Response,
    resolve: ExceptionResponse,
  ) {
    request.flash('message', resolve.message);
    response.redirect('/auth');
  }

  public httpResolve(response: Response, resolve: ExceptionResponse) {
    response.status(resolve.code).json(resolve);
  }
}
