import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import * as session from 'express-session';
import * as passport from 'passport';
import { join } from 'path';
import { AppModule } from './app.module';
import flash = require('connect-flash');
import { v4 as uuidv4 } from 'uuid';
import { ValidationPipe } from '@nestjs/common';
import { runMigration } from './common/migration';

async function bootstrap() {
  await runMigration();

  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  app.useStaticAssets(join(__dirname, '..', 'assets'));
  app.setBaseViewsDir(join(__dirname, '..', 'assets', 'views'));
  app.setViewEngine('hbs');
  app.use(
    session({
      secret: uuidv4(),
      resave: false,
      saveUninitialized: false,
    }),
  );
  app.useGlobalPipes(new ValidationPipe({ transform: true }));
  app.use(passport.initialize());
  app.use(passport.session());
  app.use(flash());
  await app.listen(3500);
}
bootstrap();
