import {
  Body,
  Controller,
  Headers,
  HttpCode,
  HttpStatus,
  Post,
} from '@nestjs/common';
import { SyncScheduleCommand } from './command/sync-schedule.command';
import { ScheduleService } from './schedule.service';

@Controller('api/schedule')
export class ScheduleController {
  constructor(private readonly scheduleService: ScheduleService) {}

  @Post()
  @HttpCode(HttpStatus.NO_CONTENT)
  public async sync(
    @Headers('Authorization') authorization: string,
    @Body() command: SyncScheduleCommand,
  ) {
    await this.scheduleService.sync(command, authorization);
  }
}
