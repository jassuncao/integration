import { nth } from 'lodash';

export function extractToken(authorization: string): string {
  return nth(authorization.split(' '), -1);
}
