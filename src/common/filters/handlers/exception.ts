import { HttpStatus } from '@nestjs/common';
import {
  HttpException,
  ForbiddenException,
  UnauthorizedException,
} from '@nestjs/common/exceptions';
import { ExceptionBuilder, ExceptionResponse } from './exception-builder';

class UnauthorizedExceptionHandler
  implements ExceptionBuilder<UnauthorizedException>
{
  build(exception: UnauthorizedException): ExceptionResponse {
    const response = exception.getResponse() as any;
    return { code: exception.getStatus(), message: response.message };
  }

  public accept(exception: HttpException): boolean {
    return exception instanceof UnauthorizedException;
  }
}

class ForbiddenExceptionHandler
  implements ExceptionBuilder<ForbiddenException>
{
  build(exception: ForbiddenException): ExceptionResponse {
    return {
      code: exception.getStatus(),
      message: 'Você não tem permissão para acessar esse recurso',
    };
  }

  public accept(exception: HttpException): boolean {
    return exception instanceof ForbiddenException;
  }
}

export interface HandleBuildException {
  build(_: Error): ExceptionResponse;
}

export const INTERNAL_ERROR: HandleBuildException = {
  build(_: Error): ExceptionResponse {
    return {
      code: HttpStatus.INTERNAL_SERVER_ERROR,
      message: 'Erro Interno',
    };
  },
};

export const EXCEPTION_BUILDERS = [
  new UnauthorizedExceptionHandler(),
  new ForbiddenExceptionHandler(),
];
