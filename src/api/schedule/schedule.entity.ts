import { Column, Entity, PrimaryColumn } from 'typeorm';

export interface ScheduleData {
  code: number;
  company: number;
  customerCode?: number;
  customerName?: string;
  schedule: Date;
  plate?: string;
  vehicleModelCode: number;
  vehicleModel: string;
  consultant: number;
}

@Entity({ name: 'ERP_API_AGENDA' })
export class Schedule {
  @PrimaryColumn({ name: 'CDAGENDAMENTOSERVICO' })
  code: number;

  @PrimaryColumn({ name: 'CDEMPRESA' })
  company: number;

  @Column({ name: 'CDCLIENTE' })
  customerCode?: number;

  @Column({ name: 'NMCLIENTE' })
  customerName?: string;

  @Column({ name: 'DTAGENDAMENTO' })
  schedule: Date;

  @Column({ name: 'DSPLACA' })
  plate?: string;

  @Column({ name: 'CDMODELOVEICULO' })
  vehicleModelCode: number;

  @Column({ name: 'DSMODELOVEICULO' })
  vehicleModel: string;

  @Column({ name: 'CDCONSULTOR' })
  consultant: number;

  static factory(data: ScheduleData): Schedule {
    const schedule = new Schedule();
    schedule.update(data);
    schedule.code = data.code;
    schedule.company = data.company;
    return schedule;
  }

  update?(data: ScheduleData): Schedule {
    this.customerCode = data.customerCode;
    this.customerName = data.customerName;
    this.schedule = data.schedule;
    this.plate = data.plate;
    this.vehicleModelCode = data.vehicleModelCode;
    this.vehicleModel = data.vehicleModel;
    this.consultant = data.consultant;
    return this;
  }
}
