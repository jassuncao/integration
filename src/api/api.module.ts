import { Module } from '@nestjs/common';
import { CommonModule } from 'src/common/common.module';
import { CompanyModule } from './company/company.module';
import { EmployeeModule } from './employee/employee.module';
import { ScheduleModule } from './schedule/schedule.module';
import { CronApiService } from './services/cron-api.service';

@Module({
  imports: [CommonModule, CompanyModule, EmployeeModule, ScheduleModule],
  providers: [CronApiService],
})
export class ApiModule {}
