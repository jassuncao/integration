import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
} from 'typeorm';

export class ScheduleCreate1651801537538 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'ERP_API_AGENDA',
        columns: [
          {
            name: 'CDAGENDAMENTOSERVICO',
            type: 'integer',
          },
          {
            name: 'CDCLIENTE',
            type: 'integer',
            isNullable: true,
          },
          {
            name: 'NMCLIENTE',
            type: 'varchar(200)',
            isNullable: true,
          },
          {
            name: 'DTAGENDAMENTO',
            type: 'date',
          },
          {
            name: 'DSPLACA',
            type: 'varchar(10)',
            isNullable: true,
          },
          {
            name: 'CDMODELOVEICULO',
            type: 'integer',
          },
          {
            name: 'DSMODELOVEICULO',
            type: 'varchar(200)',
          },
          {
            name: 'CDCONSULTOR',
            type: 'integer',
          },
          {
            name: 'CDEMPRESA',
            type: 'integer',
          },
        ],
      }),
    );

    await queryRunner.createPrimaryKey('ERP_API_AGENDA', [
      'CDAGENDAMENTOSERVICO',
      'CDEMPRESA',
    ]);

    await queryRunner.createForeignKey(
      'ERP_API_AGENDA',
      new TableForeignKey({
        name: 'ERP_AGENDA_EMPRESA_FK',
        columnNames: ['CDEMPRESA'],
        referencedTableName: 'ERP_API_EMPRESA',
        referencedColumnNames: ['CDEMPRESA'],
      }),
    );

    await queryRunner.createForeignKey(
      'ERP_API_AGENDA',
      new TableForeignKey({
        name: 'ERP_AGENDA_FUNCIONARO_FK',
        columnNames: ['CDCONSULTOR', 'CDEMPRESA'],
        referencedTableName: 'ERP_API_FUNCIONARIO',
        referencedColumnNames: ['CDVENDEDOR', 'CDEMPRESA'],
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey('ERP_API_AGENDA', 'ERP_AGENDA_EMPRESA_FK');
    await queryRunner.dropForeignKey(
      'ERP_API_AGENDA',
      'ERP_AGENDA_FUNCIONARO_FK',
    );
    await queryRunner.dropTable('ERP_API_AGENDA');
  }
}
