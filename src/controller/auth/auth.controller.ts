import {
  Controller,
  Get,
  Post,
  Render,
  Request,
  Res,
  UseGuards,
} from '@nestjs/common';
import { Response } from 'express';
import { LoginSessionGuard } from 'src/common/guards/login-session.guard';

@Controller('auth')
export class AuthController {
  @Get()
  @Render('auth')
  public index(@Request() req) {
    return {
      message: req.flash('message'),
      year: new Date().getFullYear(),
    };
  }

  @UseGuards(LoginSessionGuard)
  @Post('')
  public login(@Res() res: Response) {
    res.redirect('/schedule');
  }

  @Get('/logout')
  public logout(@Request() req, @Res() res: Response) {
    req.logout();
    res.redirect('/auth');
  }
}
