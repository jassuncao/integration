import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { extractToken } from 'src/common/authorization.helper';
import { SisandService } from 'src/common/services/sisand.service';
import { Repository } from 'typeorm';
import { Company } from '../company/company.entity';
import { CompanyService } from '../company/company.service';
import { SyncScheduleCommand } from './command/sync-schedule.command';
import { Schedule } from './schedule.entity';

type SyncSchedule = (company: Company) => Promise<Schedule[]>;

@Injectable()
export class ScheduleService {
  constructor(
    @InjectRepository(Schedule)
    private readonly scheduleRepository: Repository<Schedule>,
    private readonly companyService: CompanyService,
    private readonly sisandService: SisandService,
  ) {}

  public async sync(
    command: SyncScheduleCommand,
    authorization: string,
  ): Promise<void> {
    const companies = await this.companyService.getAll();
    const token = extractToken(authorization);
    await Promise.all(companies.map(this.syncSchedule(command, token)));
  }

  private syncSchedule = (
    command: SyncScheduleCommand,
    token: string,
  ): SyncSchedule => {
    return async ({ code }) => {
      const data = await this.sisandService.getSchedules(
        code,
        command.startDate,
        command.endDate,
        token,
      );
      const employees = data
        .map((data) => ({
          code: data.CDAGENDAMENTOSERVICO,
          company: code,
          customerCode: data.CDCLIENTE,
          customerName: data.NMCLIENTE,
          schedule: data.DTAGENDAMENTO,
          plate: data.DSPLACA,
          vehicleModelCode: data.CDMODELOVEICULO,
          vehicleModel: data.DSMODELOVEICULO,
          consultant: data.CDCONSULTOR,
        }))
        .map(Schedule.factory);
      return this.scheduleRepository.save(employees);
    };
  };
}
