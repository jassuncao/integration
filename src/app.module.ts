import { Module } from '@nestjs/common';
import { APP_FILTER } from '@nestjs/core/constants';
import { ScheduleModule } from '@nestjs/schedule';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ApiModule } from './api/api.module';
import { CommonModule } from './common/common.module';
import { HandleExceptionFilter } from './common/filters/handle-exception.filter';
import { CONNECTION_CONFIG } from './common/migration';
import { ControllerModule } from './controller/controller.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(CONNECTION_CONFIG),
    CommonModule,
    ControllerModule,
    ApiModule,
    ScheduleModule.forRoot(),
  ],
  providers: [
    {
      provide: APP_FILTER,
      useClass: HandleExceptionFilter,
    },
  ],
})
export class AppModule {}
