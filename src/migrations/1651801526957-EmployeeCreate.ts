import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
} from 'typeorm';

export class EmployeeCreate1651801526957 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'ERP_API_FUNCIONARIO',
        columns: [
          {
            name: 'CDVENDEDOR',
            type: 'integer',
          },
          {
            name: 'NMVENDEDOR',
            type: 'varchar(200)',
          },
          {
            name: 'NMAPELIDOVENDEDOR',
            type: 'varchar(50)',
            isNullable: true,
          },
          {
            name: 'CDEMPRESA',
            type: 'integer',
          },
        ],
      }),
    );

    await queryRunner.createPrimaryKey('ERP_API_FUNCIONARIO', [
      'CDVENDEDOR',
      'CDEMPRESA',
    ]);

    await queryRunner.createForeignKey(
      'ERP_API_FUNCIONARIO',
      new TableForeignKey({
        name: 'ERP_FUNCIONARO_EMPRESA_FK',
        columnNames: ['CDEMPRESA'],
        referencedTableName: 'ERP_API_EMPRESA',
        referencedColumnNames: ['CDEMPRESA'],
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey(
      'ERP_API_FUNCIONARIO',
      'ERP_FUNCIONARO_EMPRESA_FK',
    );
    await queryRunner.dropTable('ERP_API_FUNCIONARIO');
  }
}
