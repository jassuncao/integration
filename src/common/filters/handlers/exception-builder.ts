import { HttpStatus } from '@nestjs/common';
import { HttpException } from '@nestjs/common/exceptions';
export interface ExceptionResponse {
  message: any;
  code: HttpStatus;
}

export interface ExceptionBuilder<T extends HttpException> {
  build(exception: T): ExceptionResponse;

  accept(exception: HttpException): boolean;
}
