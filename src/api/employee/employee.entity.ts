import { Column, Entity, PrimaryColumn } from 'typeorm';

export interface EmployeeData {
  code: number;
  name: string;
  alias?: string;
  company: number;
}

@Entity({ name: 'ERP_API_FUNCIONARIO' })
export class Employee {
  @PrimaryColumn({ name: 'CDVENDEDOR' })
  code: number;

  @Column({ name: 'NMVENDEDOR' })
  name: string;

  @Column({ name: 'NMAPELIDOVENDEDOR' })
  alias?: string;

  @PrimaryColumn({ name: 'CDEMPRESA' })
  company: number;

  static factory(data: EmployeeData): Employee {
    const employee = new Employee();
    employee.update(data);
    employee.code = data.code;
    employee.company = data.company;
    return employee;
  }

  update?(data: EmployeeData): Employee {
    this.name = data.name;
    this.alias = data.alias;
    return this;
  }
}
