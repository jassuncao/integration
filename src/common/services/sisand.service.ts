import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { lightFormat } from 'date-fns';
import { catchError, firstValueFrom, map, of, take } from 'rxjs';
@Injectable()
export class SisandService {
  private readonly baseUrl =
    'http://grupoventura.sisand.com.br/VisionIntegracao/api';

  constructor(private readonly http: HttpService) {}

  async getToken(username: string, pass: string): Promise<any> {
    const params = { DSLOGIN: username, DSSENHA: pass };
    const resource = `${this.baseUrl}/Login/Entrar`;
    const request$ = this.http.get(resource, { params }).pipe(
      catchError(() => of({ data: { token: undefined } })),
      map(({ data }) => data.token),
      take(1),
    );
    return firstValueFrom(request$);
  }

  async getCompanies(token: string): Promise<any[]> {
    const resource = `${this.baseUrl}/TabelaAuxiliar/GetEmpresa?token=${token}`;
    const request$ = this.http.get(resource).pipe(
      catchError(() => of({ data: undefined })),
      map(({ data }) => data),
      take(1),
    );
    return firstValueFrom(request$);
  }

  async getEmployees(company: number, token: string): Promise<any[]> {
    const resource = `${this.baseUrl}/Funcionario/GetVendedorEmpresa?token=${token}&cdEmpresa=${company}`;
    const request$ = this.http.get(resource).pipe(
      catchError(() => of({ data: undefined })),
      map(({ data }) => data),
      take(1),
    );
    return firstValueFrom(request$);
  }

  async getSchedules(
    company: number,
    start: Date,
    end: Date,
    token: string,
  ): Promise<any[]> {
    const startFormatted = lightFormat(start, 'dd/MM/yyyy');
    const endFormatted = lightFormat(end, 'dd/MM/yyyy');
    const resource = `${this.baseUrl}/Oficina/ObterAgendamentos?token=${token}&cdEmpresa=${company}&DTINICIO=${startFormatted}&DTFIM=${endFormatted}`;
    const request$ = this.http.get(resource).pipe(
      catchError(() => of({ data: undefined })),
      map(({ data }) => data),
      take(1),
    );
    return firstValueFrom(request$);
  }
}
