import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-local';
import { SisandService } from './services/sisand.service';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly sisandService: SisandService) {
    super();
  }

  async validate(username: string, password: string) {
    const token = await this.sisandService.getToken(username, password);
    if (!token) {
      throw new UnauthorizedException('Credenciais erradas');
    }
    return { username, token };
  }
}
