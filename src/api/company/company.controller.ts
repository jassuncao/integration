import {
  Controller,
  Headers,
  HttpCode,
  HttpStatus,
  Post,
} from '@nestjs/common';
import { CompanyService } from './company.service';

@Controller('api/company')
export class CompanyController {
  constructor(private readonly companyService: CompanyService) {}

  @Post()
  @HttpCode(HttpStatus.NO_CONTENT)
  public async sync(@Headers('Authorization') authorization: string) {
    await this.companyService.sync(authorization);
  }
}