import { ConnectionOptions, createConnection } from 'typeorm';

export const CONNECTION_CONFIG: ConnectionOptions = {
  type: 'oracle',
  host: 'localhost',
  port: 49161,
  username: 'system',
  password: 'oracle',
  database: 'xe',
  synchronize: false,
  logging: false,
  entities: ['{dist, src}/api/**/*.entity{.ts,.js}'],
  migrations: ['{dist, src}/migrations/*{.ts,.js}'],
};

export async function runMigration() {
  const connection = await createConnection(CONNECTION_CONFIG);
  return connection.runMigrations().finally(() => connection.close());
}
